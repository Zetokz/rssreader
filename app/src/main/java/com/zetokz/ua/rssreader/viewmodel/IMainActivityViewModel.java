package com.zetokz.ua.rssreader.viewmodel;

public interface IMainActivityViewModel {
    void destroy();
    void openSettings();
}
