package com.zetokz.ua.rssreader.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import com.zetokz.ua.rssreader.BR;

import org.parceler.Parcel;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Parcel
@Root(name = "item", strict = false)
public class RssItem extends BaseObservable {

    public RssItem() {}

    @Element(name = "title", data = true, required = false)
    public String title;
    @Element(name = "link", required = false)
    public String link;
    @Element(name = "pubDate", required = false)
    public String date;
    @Element(name = "author", required = false)
    public String author;
    @Element(name = "description", data = true, required = false)
    public String description;
    @Element(required = false)
    @Bindable
    public String picture;

    public void setPicture(final String _picture) {
        picture = _picture;
        notifyPropertyChanged(BR.picture);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RssItem rssItem = (RssItem) o;

        if (title != null ? !title.equals(rssItem.title) : rssItem.title != null) return false;
        if (link != null ? !link.equals(rssItem.link) : rssItem.link != null) return false;
        if (date != null ? !date.equals(rssItem.date) : rssItem.date != null) return false;
        if (author != null ? !author.equals(rssItem.author) : rssItem.author != null) return false;
        if (description != null ? !description.equals(rssItem.description) : rssItem.description != null)
            return false;
        return !(picture != null ? !picture.equals(rssItem.picture) : rssItem.picture != null);
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (picture != null ? picture.hashCode() : 0);
        return result;
    }
}