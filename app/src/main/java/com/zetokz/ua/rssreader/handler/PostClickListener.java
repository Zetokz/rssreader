package com.zetokz.ua.rssreader.handler;

import com.zetokz.ua.rssreader.model.RssItem;

public interface PostClickListener {
    void onPostClick(RssItem item);
}
