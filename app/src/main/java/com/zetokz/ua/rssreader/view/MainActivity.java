package com.zetokz.ua.rssreader.view;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import com.zetokz.ua.rssreader.R;
import com.zetokz.ua.rssreader.databinding.ActivityMainBinding;
import com.zetokz.ua.rssreader.viewmodel.IMainActivityViewModel;
import com.zetokz.ua.rssreader.viewmodel.MainActivityViewModelImpl;

public class MainActivity extends AppCompatActivity implements DataListener {

    private ActivityMainBinding mBinding;
    private IMainActivityViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mViewModel = new MainActivityViewModelImpl(this, this);
//        mBinding.setVariable(BR.mainActivityViewModel, mViewModel);
        mBinding.setMainActivityViewModel((MainActivityViewModelImpl) mViewModel);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        mViewModel.openSettings();
        return false;
    }

    @Override
    protected void onDestroy() {
        mBinding.unbind(); // if this need for you, may have problems on api older than 17
        super.onDestroy();
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
}
