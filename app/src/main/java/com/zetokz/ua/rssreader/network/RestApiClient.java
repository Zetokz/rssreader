package com.zetokz.ua.rssreader.network;


import com.zetokz.ua.rssreader.model.Rss;

import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import retrofit.SimpleXmlConverterFactory;
import retrofit.http.GET;
import retrofit.http.Url;
import rx.Observable;

public interface RestApiClient {

    @GET
    Observable<Rss> getTopStories(@Url String url);

    class Builder {
        public static RestApiClient create() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RestApiConfig.BASE_URL)
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(RestApiClient.class);
        }
    }
}
