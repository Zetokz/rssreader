package com.zetokz.ua.rssreader;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * Created by zetokz on 12/9/2015.
 */
public class MyImageView extends ImageView {

    public MyImageView(final Context context) {
        super(context);
    }

    public MyImageView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public MyImageView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void load(final String _url) {
        Glide.with(getContext())
                .load(_url)
                .fitCenter()
                .into(this);
    }
}
