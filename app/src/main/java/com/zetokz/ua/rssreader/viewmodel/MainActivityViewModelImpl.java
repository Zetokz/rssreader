package com.zetokz.ua.rssreader.viewmodel;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.CharacterPickerDialog;
import android.view.View;

import com.zetokz.ua.rssreader.adapter.AllPostsAdapter;
import com.zetokz.ua.rssreader.handler.PostHandler;
import com.zetokz.ua.rssreader.model.Rss;
import com.zetokz.ua.rssreader.model.RssItem;
import com.zetokz.ua.rssreader.network.RestApiClient;
import com.zetokz.ua.rssreader.network.RestApiConfig;
import com.zetokz.ua.rssreader.utils.BindableBoolean;
import com.zetokz.ua.rssreader.view.DataListener;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivityViewModelImpl implements IMainActivityViewModel{

    public ObservableBoolean isEmptyMessageVisible = new ObservableBoolean(false);
    public ObservableBoolean isProgressVisible = new ObservableBoolean(false);
    public ObservableBoolean preferenceVisibility = new ObservableBoolean(false);
    public BindableBoolean isRefreshing = new BindableBoolean();
    private List<RssItem> mFilteredRss = new ArrayList<>();

    private DataListener mDataListener;

    public AllPostsAdapter bindableAdapter;

    private PostHandler mPostHandler;

    private Subscription mSubscription;
    private List<RssItem> mRssItems;

    public MainActivityViewModelImpl(final Context _context, final DataListener _dataListener) {
        mPostHandler = new PostHandler(_context);
        mDataListener = _dataListener;
        bindableAdapter = new AllPostsAdapter(mPostHandler, new ArrayList<RssItem>());
        isProgressVisible.set(true);
        retrieveData();
    }

    protected void retrieveData() {
        isProgressVisible.set(true);
        if (mSubscription != null && !mSubscription.isUnsubscribed()) mSubscription.unsubscribe();
        mSubscription = getRequestObservable()
                .subscribe(new Subscriber<Rss>() {
                    @Override
                    public void onCompleted() {
                        isEmptyMessageVisible.set(false);
                        isProgressVisible.set(false);
                        isRefreshing.set(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        isEmptyMessageVisible.set(true);
                        isProgressVisible.set(false);
                        isRefreshing.set(false);
                    }

                    @Override
                    public void onNext(Rss _response) {
                        mRssItems = _response.getChannel().getItemList();
                        bindableAdapter.updateData(_response.getChannel().getItemList());
                    }
                });
    }

    public Observable<Rss> getRequestObservable() {
        return RestApiClient.Builder
                .create()
                .getTopStories(RestApiConfig.BASE_URL)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    public void onRefresh() {
        isRefreshing.set(true);
        isEmptyMessageVisible.set(false);
        bindableAdapter.clear();
        retrieveData();
    }

    @Override
    public void destroy() {
        if (!mSubscription.isUnsubscribed()) mSubscription.unsubscribe();
        bindableAdapter = null;
        mPostHandler = null;
        isEmptyMessageVisible = null;
        isProgressVisible = null;
        isRefreshing = null;
    }

    @Override
    public void openSettings() {
        preferenceVisibility.set(!preferenceVisibility.get());
    }

    public TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {

        }

        @Override
        public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {

        }

        @Override
        public void afterTextChanged(final Editable s) {
            showSurprise(s.toString());
            filterData(s.toString());
            bindableAdapter.updateData(mFilteredRss);
        }
    };
    private void filterData(final String _text) {
        if (mRssItems == null) return;
        mFilteredRss.clear();
        for (int i = 0; i < mRssItems.size(); i++) {
            final RssItem item = mRssItems.get(i);
            if (item.title.toLowerCase().contains(_text.toLowerCase())) mFilteredRss.add(item);
        }
    }

    private void showSurprise(final String _text) {
        if (_text.equals("surprise")) {
            surpriseVisibility.set(true);
            mDataListener.hideKeyboard();
            return;
        }
    }

    public ObservableBoolean surpriseVisibility = new ObservableBoolean(false);
    public ObservableField<String> surprise = new ObservableField<>("https://media.giphy.com/media/IyF2qP8x3KuXu/giphy.gif");
    public View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            mDataListener.hideKeyboard();
            surpriseVisibility.set(false);
        }
    };
}
