package com.zetokz.ua.rssreader.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zetokz.ua.rssreader.BR;
import com.zetokz.ua.rssreader.R;
import com.zetokz.ua.rssreader.handler.PostHandler;
import com.zetokz.ua.rssreader.model.Rss;
import com.zetokz.ua.rssreader.model.RssItem;
import com.zetokz.ua.rssreader.viewholder.BindingHolder;

import java.util.List;

public class AllPostsAdapter extends BindableAdapter<RssItem> {

    public PostHandler mPostHandler;

    public AllPostsAdapter(final PostHandler _postHandler,
                           final List<RssItem> _loadedModels) {
        mPostHandler = _postHandler;
        mModelList = _loadedModels;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_simple_post, parent, false);
        return new BindingHolder(v);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        holder.getBinding().setVariable(BR.rssItem, mModelList.get(position));
        holder.getBinding().setVariable(BR.postHandler, mPostHandler);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mModelList == null ? 0 : mModelList.size();
    }

    public void addData(List<RssItem> rss) {
        mModelList.addAll(rss);
        notifyDataSetChanged();
    }

    public void updateData(List<RssItem> rss) {
        mModelList.clear();
        mModelList.addAll(rss);
        notifyDataSetChanged();
    }

    public void clear() {
        mModelList.clear();
        notifyDataSetChanged();
    }
}
