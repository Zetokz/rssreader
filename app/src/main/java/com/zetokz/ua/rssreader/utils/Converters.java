package com.zetokz.ua.rssreader.utils;

import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.databinding.BindingMethod;
import android.databinding.BindingMethods;
import android.databinding.ObservableBoolean;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.method.CharacterPickerDialog;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.zetokz.ua.rssreader.adapter.BindableAdapter;
import com.zetokz.ua.rssreader.handler.PostClickListener;
import com.zetokz.ua.rssreader.model.RssItem;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Converters {

    @BindingMethod(type = com.zetokz.ua.rssreader.MyImageView.class,
            attribute = "app:load",
            method = "load")

    @BindingConversion
    public static int convertBooleanToInt(boolean value) {
        return value ? View.VISIBLE : View.GONE;
    }

//    @BindingAdapter({"onClickSwitcher"})
//    public static void onClickSwitcher(final View _view, final ObservableBoolean b) {
//        _view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View v) {
//                b.set(!b.get());
//            }
//        });
//    }

    @BindingAdapter({"bind:adapter"})
    public static void bindOnSetAdapter(final RecyclerView _view, final BindableAdapter _adapter) {
        if (_view.getAdapter() == null)
            _view.setAdapter(_adapter);
    }

    @BindingAdapter({"bind:isRefreshing"})
    public static void setRefreshing(final SwipeRefreshLayout _view, final BindableBoolean _isRefreshing) {
        _view.setRefreshing(_isRefreshing.get());

    }

    @BindingAdapter({"bind:loadImage"})
    public static void loadUserAvatar(final ImageView _imageView, final String _description) {
        String IMG_REGEX = "<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>";
        final Pattern IMG_PATTERN = Pattern.compile(IMG_REGEX);
        final Matcher matcher = IMG_PATTERN.matcher(_description);
        String result = "";
        if (matcher.find()) {
            result = matcher.group(1);
        }

        Glide.with(_imageView.getContext())
                .load(result)
                .crossFade()
                .centerCrop()
                .into(_imageView);
    }

    @BindingAdapter({"bind:handler", "bind:post"})
    public static void onFollowButtonClick(final View _view,
                                           final PostClickListener _listener,
                                           final RssItem _item) {
        _view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _listener.onPostClick(_item);
            }
        });
    }
}
