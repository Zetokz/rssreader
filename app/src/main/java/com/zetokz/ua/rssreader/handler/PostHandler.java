package com.zetokz.ua.rssreader.handler;

import android.content.Context;
import android.content.Intent;

import com.zetokz.ua.rssreader.model.RssItem;
import com.zetokz.ua.rssreader.view.DetailsActivity;

import rx.Subscription;

public class PostHandler implements PostClickListener {
    protected Context mContext;
    protected Subscription mSubscription;
    public PostHandler(final Context _context) {
        mContext = _context;
    }

    public PostHandler() { }

    public void setContext(final Context _context) {
        mContext = _context;
    }

    @Override
    public void onPostClick(RssItem item) {
        Intent newActivity = new Intent(mContext, DetailsActivity.class);
        newActivity.putExtra("link", item.link);
        mContext.startActivity(newActivity);
    }
}
